﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.TimerCountAndCheck = New System.Windows.Forms.Timer(Me.components)
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Datagrid_Jadwal = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Btn_CopyData = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnBaru = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.btn_PengNmSkolah = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btn_restoreDB = New System.Windows.Forms.Button()
        Me.btn_backup = New System.Windows.Forms.Button()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.LinkLabel3 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.version_Software = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Lbl_Jl = New System.Windows.Forms.Label()
        Me.LbL_NamaSkol = New System.Windows.Forms.Label()
        Me.PictApp = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.GetHariTxt = New System.Windows.Forms.Label()
        Me.TimerCount1 = New System.Windows.Forms.Label()
        Me.LinkLabel4 = New System.Windows.Forms.LinkLabel()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.Datagrid_Jadwal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.PictApp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TimerCountAndCheck
        '
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(8, 115)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(306, 427)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.Datagrid_Jadwal)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 26)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(298, 397)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Jadwal"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.RosyBrown
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(12, 261)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 17)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Result Count : 0"
        '
        'Datagrid_Jadwal
        '
        Me.Datagrid_Jadwal.AllowUserToAddRows = False
        Me.Datagrid_Jadwal.AllowUserToDeleteRows = False
        Me.Datagrid_Jadwal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.Datagrid_Jadwal.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.Datagrid_Jadwal.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Datagrid_Jadwal.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.Datagrid_Jadwal.Location = New System.Drawing.Point(10, 9)
        Me.Datagrid_Jadwal.Name = "Datagrid_Jadwal"
        Me.Datagrid_Jadwal.RowHeadersVisible = False
        Me.Datagrid_Jadwal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Datagrid_Jadwal.Size = New System.Drawing.Size(276, 249)
        Me.Datagrid_Jadwal.TabIndex = 7
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.Btn_CopyData)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.btnBaru)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox1.ForeColor = System.Drawing.Color.Thistle
        Me.GroupBox1.Location = New System.Drawing.Point(6, 277)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(289, 117)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Action"
        '
        'Btn_CopyData
        '
        Me.Btn_CopyData.BackColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.Btn_CopyData.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_CopyData.FlatAppearance.BorderColor = System.Drawing.Color.MediumSeaGreen
        Me.Btn_CopyData.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(205, Byte), Integer), CType(CType(225, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.Btn_CopyData.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkSeaGreen
        Me.Btn_CopyData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Btn_CopyData.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_CopyData.ForeColor = System.Drawing.SystemColors.Control
        Me.Btn_CopyData.Image = CType(resources.GetObject("Btn_CopyData.Image"), System.Drawing.Image)
        Me.Btn_CopyData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Btn_CopyData.Location = New System.Drawing.Point(193, 20)
        Me.Btn_CopyData.Name = "Btn_CopyData"
        Me.Btn_CopyData.Size = New System.Drawing.Size(92, 42)
        Me.Btn_CopyData.TabIndex = 6
        Me.Btn_CopyData.Text = "Copy"
        Me.Btn_CopyData.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Btn_CopyData.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Red
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.LightSlateGray
        Me.Button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.Button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.SystemColors.Control
        Me.Button2.Image = CType(resources.GetObject("Button2.Image"), System.Drawing.Image)
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(194, 68)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(92, 42)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Hapus"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(29, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.BlueViolet
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(177, Byte), Integer), CType(CType(114, Byte), Integer), CType(CType(235, Byte), Integer))
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.BlueViolet
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.SystemColors.Control
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(99, 20)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(92, 42)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Ubah"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnBaru
        '
        Me.btnBaru.BackColor = System.Drawing.Color.MediumSlateBlue
        Me.btnBaru.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnBaru.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue
        Me.btnBaru.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(235, Byte), Integer))
        Me.btnBaru.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue
        Me.btnBaru.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBaru.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBaru.ForeColor = System.Drawing.SystemColors.Control
        Me.btnBaru.Image = CType(resources.GetObject("btnBaru.Image"), System.Drawing.Image)
        Me.btnBaru.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBaru.Location = New System.Drawing.Point(4, 20)
        Me.btnBaru.Name = "btnBaru"
        Me.btnBaru.Size = New System.Drawing.Size(92, 42)
        Me.btnBaru.TabIndex = 3
        Me.btnBaru.Text = "Add"
        Me.btnBaru.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnBaru.UseVisualStyleBackColor = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.TabPage2.Controls.Add(Me.Panel2)
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Controls.Add(Me.GroupBox2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 26)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(298, 397)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Pengaturan"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.LightGray
        Me.Panel2.Location = New System.Drawing.Point(16, 185)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(266, 1)
        Me.Panel2.TabIndex = 5
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button4)
        Me.GroupBox3.Controls.Add(Me.btn_PengNmSkolah)
        Me.GroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.GroupBox3.Location = New System.Drawing.Point(19, 203)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(260, 142)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Pengaturan Internal"
        '
        'Button4
        '
        Me.Button4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button4.FlatAppearance.BorderColor = System.Drawing.Color.SeaGreen
        Me.Button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(11, Byte), Integer), CType(CType(21, Byte), Integer))
        Me.Button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(5, Byte), Integer), CType(CType(10, Byte), Integer))
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.LightSteelBlue
        Me.Button4.Image = CType(resources.GetObject("Button4.Image"), System.Drawing.Image)
        Me.Button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button4.Location = New System.Drawing.Point(93, 79)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(161, 49)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "Ganti Logo"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button4.UseVisualStyleBackColor = True
        '
        'btn_PengNmSkolah
        '
        Me.btn_PengNmSkolah.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_PengNmSkolah.FlatAppearance.BorderColor = System.Drawing.Color.DarkTurquoise
        Me.btn_PengNmSkolah.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(11, Byte), Integer), CType(CType(21, Byte), Integer))
        Me.btn_PengNmSkolah.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(5, Byte), Integer), CType(CType(10, Byte), Integer))
        Me.btn_PengNmSkolah.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_PengNmSkolah.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_PengNmSkolah.ForeColor = System.Drawing.Color.MediumPurple
        Me.btn_PengNmSkolah.Image = CType(resources.GetObject("btn_PengNmSkolah.Image"), System.Drawing.Image)
        Me.btn_PengNmSkolah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_PengNmSkolah.Location = New System.Drawing.Point(7, 24)
        Me.btn_PengNmSkolah.Name = "btn_PengNmSkolah"
        Me.btn_PengNmSkolah.Size = New System.Drawing.Size(161, 49)
        Me.btn_PengNmSkolah.TabIndex = 2
        Me.btn_PengNmSkolah.Text = "Pengaturan Nama Sekolah"
        Me.btn_PengNmSkolah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_PengNmSkolah.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btn_restoreDB)
        Me.GroupBox2.Controls.Add(Me.btn_backup)
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox2.ForeColor = System.Drawing.Color.Thistle
        Me.GroupBox2.Location = New System.Drawing.Point(7, 52)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(284, 112)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Pengaturan Database"
        '
        'btn_restoreDB
        '
        Me.btn_restoreDB.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_restoreDB.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(11, Byte), Integer), CType(CType(21, Byte), Integer))
        Me.btn_restoreDB.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(5, Byte), Integer), CType(CType(10, Byte), Integer))
        Me.btn_restoreDB.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_restoreDB.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_restoreDB.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn_restoreDB.Image = CType(resources.GetObject("btn_restoreDB.Image"), System.Drawing.Image)
        Me.btn_restoreDB.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_restoreDB.Location = New System.Drawing.Point(154, 31)
        Me.btn_restoreDB.Name = "btn_restoreDB"
        Me.btn_restoreDB.Size = New System.Drawing.Size(112, 57)
        Me.btn_restoreDB.TabIndex = 1
        Me.btn_restoreDB.Text = "Restore Database"
        Me.btn_restoreDB.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_restoreDB.UseVisualStyleBackColor = True
        '
        'btn_backup
        '
        Me.btn_backup.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_backup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(11, Byte), Integer), CType(CType(21, Byte), Integer))
        Me.btn_backup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(5, Byte), Integer), CType(CType(10, Byte), Integer))
        Me.btn_backup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_backup.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_backup.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.btn_backup.Image = CType(resources.GetObject("btn_backup.Image"), System.Drawing.Image)
        Me.btn_backup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_backup.Location = New System.Drawing.Point(19, 31)
        Me.btn_backup.Name = "btn_backup"
        Me.btn_backup.Size = New System.Drawing.Size(112, 57)
        Me.btn_backup.TabIndex = 0
        Me.btn_backup.Text = "Backup Database"
        Me.btn_backup.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_backup.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage3.Controls.Add(Me.LinkLabel4)
        Me.TabPage3.Controls.Add(Me.LinkLabel3)
        Me.TabPage3.Controls.Add(Me.LinkLabel2)
        Me.TabPage3.Controls.Add(Me.LinkLabel1)
        Me.TabPage3.Controls.Add(Me.RichTextBox1)
        Me.TabPage3.Controls.Add(Me.version_Software)
        Me.TabPage3.Controls.Add(Me.Label2)
        Me.TabPage3.Controls.Add(Me.PictureBox1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 26)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(298, 397)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Tentang"
        '
        'LinkLabel3
        '
        Me.LinkLabel3.AutoSize = True
        Me.LinkLabel3.Location = New System.Drawing.Point(253, 363)
        Me.LinkLabel3.Name = "LinkLabel3"
        Me.LinkLabel3.Size = New System.Drawing.Size(39, 17)
        Me.LinkLabel3.TabIndex = 6
        Me.LinkLabel3.TabStop = True
        Me.LinkLabel3.Text = "Email"
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.Location = New System.Drawing.Point(191, 363)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(56, 17)
        Me.LinkLabel2.TabIndex = 5
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Youtube"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(121, 363)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(64, 17)
        Me.LinkLabel1.TabIndex = 4
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Facebook"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Font = New System.Drawing.Font("Segoe UI Semibold", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox1.ForeColor = System.Drawing.Color.SlateBlue
        Me.RichTextBox1.Location = New System.Drawing.Point(3, 201)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.ReadOnly = True
        Me.RichTextBox1.Size = New System.Drawing.Size(289, 154)
        Me.RichTextBox1.TabIndex = 3
        Me.RichTextBox1.Text = "Software Dev :" & Global.Microsoft.VisualBasic.ChrW(10) & "PoetralesanA { 571M Corp }" & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(10) & "Thanks For My Friend :" & Global.Microsoft.VisualBasic.ChrW(10) & "- Root16" & Global.Microsoft.VisualBasic.ChrW(10) & "- Riza" & _
    "ldi Salim ( Java Coder )" & Global.Microsoft.VisualBasic.ChrW(10) & "- Afat Rasta" & Global.Microsoft.VisualBasic.ChrW(10) & "- Fiteri" & Global.Microsoft.VisualBasic.ChrW(10) & "- Irshan" & Global.Microsoft.VisualBasic.ChrW(10) & "- Imha" & Global.Microsoft.VisualBasic.ChrW(10) & "- Petak 6 Family"
        '
        'version_Software
        '
        Me.version_Software.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.version_Software.Font = New System.Drawing.Font("Lucida Fax", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.version_Software.ForeColor = System.Drawing.Color.DarkMagenta
        Me.version_Software.Location = New System.Drawing.Point(68, 141)
        Me.version_Software.Name = "version_Software"
        Me.version_Software.Size = New System.Drawing.Size(163, 43)
        Me.version_Software.TabIndex = 2
        Me.version_Software.Text = "GetVersion()"
        Me.version_Software.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Lucida Console", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Indigo
        Me.Label2.Location = New System.Drawing.Point(18, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(261, 19)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Aplikasi Bell Sekolah"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Aplikasi_Bel_Sekolah_Otomatis_by_571Mcorp.My.Resources.Resources.Bacground_Pict
        Me.PictureBox1.Location = New System.Drawing.Point(68, 44)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(163, 94)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Lbl_Jl)
        Me.Panel1.Controls.Add(Me.LbL_NamaSkol)
        Me.Panel1.Controls.Add(Me.PictApp)
        Me.Panel1.Location = New System.Drawing.Point(8, 8)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(305, 61)
        Me.Panel1.TabIndex = 1
        '
        'Lbl_Jl
        '
        Me.Lbl_Jl.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_Jl.ForeColor = System.Drawing.SystemColors.InactiveBorder
        Me.Lbl_Jl.Location = New System.Drawing.Point(92, 28)
        Me.Lbl_Jl.Name = "Lbl_Jl"
        Me.Lbl_Jl.Size = New System.Drawing.Size(210, 20)
        Me.Lbl_Jl.TabIndex = 2
        Me.Lbl_Jl.Text = "GET JALAN SEKOLAH()"
        Me.Lbl_Jl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LbL_NamaSkol
        '
        Me.LbL_NamaSkol.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LbL_NamaSkol.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbL_NamaSkol.ForeColor = System.Drawing.Color.Snow
        Me.LbL_NamaSkol.Location = New System.Drawing.Point(92, 4)
        Me.LbL_NamaSkol.Name = "LbL_NamaSkol"
        Me.LbL_NamaSkol.Size = New System.Drawing.Size(210, 22)
        Me.LbL_NamaSkol.TabIndex = 1
        Me.LbL_NamaSkol.Text = "GET NAMA SEKOLAH()"
        Me.LbL_NamaSkol.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictApp
        '
        Me.PictApp.Location = New System.Drawing.Point(3, 4)
        Me.PictApp.Name = "PictApp"
        Me.PictApp.Size = New System.Drawing.Size(83, 51)
        Me.PictApp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictApp.TabIndex = 0
        Me.PictApp.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.Panel3.Controls.Add(Me.GetHariTxt)
        Me.Panel3.Controls.Add(Me.TimerCount1)
        Me.Panel3.Location = New System.Drawing.Point(8, 71)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(305, 43)
        Me.Panel3.TabIndex = 2
        '
        'GetHariTxt
        '
        Me.GetHariTxt.AutoSize = True
        Me.GetHariTxt.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GetHariTxt.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.GetHariTxt.Location = New System.Drawing.Point(10, 21)
        Me.GetHariTxt.Name = "GetHariTxt"
        Me.GetHariTxt.Size = New System.Drawing.Size(75, 20)
        Me.GetHariTxt.TabIndex = 1
        Me.GetHariTxt.Text = "GetTanggal()"
        Me.GetHariTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.GetHariTxt.UseCompatibleTextRendering = True
        '
        'TimerCount1
        '
        Me.TimerCount1.AutoSize = True
        Me.TimerCount1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TimerCount1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.TimerCount1.Location = New System.Drawing.Point(10, 3)
        Me.TimerCount1.Name = "TimerCount1"
        Me.TimerCount1.Size = New System.Drawing.Size(66, 20)
        Me.TimerCount1.TabIndex = 0
        Me.TimerCount1.Text = "GetWaktu()"
        Me.TimerCount1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.TimerCount1.UseCompatibleTextRendering = True
        '
        'LinkLabel4
        '
        Me.LinkLabel4.AutoSize = True
        Me.LinkLabel4.Location = New System.Drawing.Point(8, 363)
        Me.LinkLabel4.Name = "LinkLabel4"
        Me.LinkLabel4.Size = New System.Drawing.Size(56, 17)
        Me.LinkLabel4.TabIndex = 7
        Me.LinkLabel4.TabStop = True
        Me.LinkLabel4.Text = "Donate?"
        '
        'FormMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(323, 549)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "FormMain"
        Me.Opacity = 0.99R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bell Sekolah v.01 by 571M Corp"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.Datagrid_Jadwal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictApp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TimerCountAndCheck As System.Windows.Forms.Timer
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents btnBaru As System.Windows.Forms.Button
    Friend WithEvents Datagrid_Jadwal As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Lbl_Jl As System.Windows.Forms.Label
    Friend WithEvents LbL_NamaSkol As System.Windows.Forms.Label
    Friend WithEvents PictApp As System.Windows.Forms.PictureBox
    Friend WithEvents btn_backup As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_PengNmSkolah As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents TimerCount1 As System.Windows.Forms.Label
    Friend WithEvents GetHariTxt As System.Windows.Forms.Label
    Friend WithEvents Btn_CopyData As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btn_restoreDB As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents version_Software As System.Windows.Forms.Label
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents LinkLabel3 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel4 As System.Windows.Forms.LinkLabel

End Class
