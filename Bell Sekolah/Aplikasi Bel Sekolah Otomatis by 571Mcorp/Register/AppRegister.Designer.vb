﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AppRegister
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AppRegister))
        Me.RegText = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RegBtn = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.RegMsgKey = New System.Windows.Forms.Label()
        Me.RegTextHWID = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.getNameApp = New System.Windows.Forms.Label()
        Me.WaitTimer = New System.Windows.Forms.Timer(Me.components)
        Me.RegLabelCountDown = New System.Windows.Forms.Label()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RegText
        '
        Me.RegText.BackColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.RegText.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RegText.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RegText.ForeColor = System.Drawing.Color.Gainsboro
        Me.RegText.Location = New System.Drawing.Point(11, 334)
        Me.RegText.Name = "RegText"
        Me.RegText.Size = New System.Drawing.Size(395, 20)
        Me.RegText.TabIndex = 0
        Me.RegText.Text = "Insert Key Register.."
        Me.RegText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(11, 363)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(395, 1)
        Me.Panel1.TabIndex = 1
        '
        'RegBtn
        '
        Me.RegBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RegBtn.FlatAppearance.BorderColor = System.Drawing.Color.CadetBlue
        Me.RegBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RegBtn.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RegBtn.ForeColor = System.Drawing.Color.White
        Me.RegBtn.Location = New System.Drawing.Point(8, 381)
        Me.RegBtn.Name = "RegBtn"
        Me.RegBtn.Size = New System.Drawing.Size(400, 43)
        Me.RegBtn.TabIndex = 1
        Me.RegBtn.Text = "Check"
        Me.RegBtn.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label1.Location = New System.Drawing.Point(55, 164)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(313, 30)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Untuk dapat menggunakan aplikasi" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "silahkan masukan keyregister yang telah anda da" & _
    "patkan."
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(11, Byte), Integer), CType(CType(21, Byte), Integer))
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(11, Byte), Integer), CType(CType(21, Byte), Integer))
        Me.Button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(3, Byte), Integer), CType(CType(3, Byte), Integer), CType(CType(7, Byte), Integer))
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Red
        Me.Button2.Location = New System.Drawing.Point(385, 0)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(38, 32)
        Me.Button2.TabIndex = 6
        Me.Button2.TabStop = False
        Me.Button2.Text = "X"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'RegMsgKey
        '
        Me.RegMsgKey.BackColor = System.Drawing.Color.LightSlateGray
        Me.RegMsgKey.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RegMsgKey.ForeColor = System.Drawing.Color.Lime
        Me.RegMsgKey.Location = New System.Drawing.Point(0, 221)
        Me.RegMsgKey.Name = "RegMsgKey"
        Me.RegMsgKey.Size = New System.Drawing.Size(423, 20)
        Me.RegMsgKey.TabIndex = 8
        Me.RegMsgKey.Text = "Msg Key()"
        Me.RegMsgKey.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RegMsgKey.Visible = False
        '
        'RegTextHWID
        '
        Me.RegTextHWID.BackColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.RegTextHWID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RegTextHWID.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RegTextHWID.ForeColor = System.Drawing.Color.LawnGreen
        Me.RegTextHWID.Location = New System.Drawing.Point(8, 30)
        Me.RegTextHWID.Name = "RegTextHWID"
        Me.RegTextHWID.ReadOnly = True
        Me.RegTextHWID.Size = New System.Drawing.Size(395, 20)
        Me.RegTextHWID.TabIndex = 0
        Me.RegTextHWID.TabStop = False
        Me.RegTextHWID.Text = "GetHwid()"
        Me.RegTextHWID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label2.Location = New System.Drawing.Point(129, -2)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(153, 21)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "_-= Machine ID =-_"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.RegTextHWID)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 244)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(410, 63)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        '
        'getNameApp
        '
        Me.getNameApp.BackColor = System.Drawing.Color.FromArgb(CType(CType(9, Byte), Integer), CType(CType(11, Byte), Integer), CType(CType(21, Byte), Integer))
        Me.getNameApp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.getNameApp.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.getNameApp.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.getNameApp.Location = New System.Drawing.Point(0, 0)
        Me.getNameApp.Name = "getNameApp"
        Me.getNameApp.Size = New System.Drawing.Size(423, 32)
        Me.getNameApp.TabIndex = 0
        Me.getNameApp.Text = "GetAppName()"
        Me.getNameApp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'WaitTimer
        '
        '
        'RegLabelCountDown
        '
        Me.RegLabelCountDown.BackColor = System.Drawing.Color.Transparent
        Me.RegLabelCountDown.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RegLabelCountDown.ForeColor = System.Drawing.Color.Crimson
        Me.RegLabelCountDown.Location = New System.Drawing.Point(3, 201)
        Me.RegLabelCountDown.Name = "RegLabelCountDown"
        Me.RegLabelCountDown.Size = New System.Drawing.Size(87, 20)
        Me.RegLabelCountDown.TabIndex = 12
        Me.RegLabelCountDown.Text = "Please Wait : 5"
        Me.RegLabelCountDown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.RegLabelCountDown.Visible = False
        '
        'LinkLabel1
        '
        Me.LinkLabel1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LinkLabel1.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.Image = CType(resources.GetObject("LinkLabel1.Image"), System.Drawing.Image)
        Me.LinkLabel1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LinkLabel1.Location = New System.Drawing.Point(10, 432)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(153, 25)
        Me.LinkLabel1.TabIndex = 2
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Request Key Register"
        Me.LinkLabel1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Snow
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(160, 49)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(103, 102)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'AppRegister
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(423, 471)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.getNameApp)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.RegMsgKey)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.RegBtn)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.RegText)
        Me.Controls.Add(Me.RegLabelCountDown)
        Me.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "AppRegister"
        Me.Opacity = 0.95R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RegText As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents RegBtn As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents RegMsgKey As System.Windows.Forms.Label
    Friend WithEvents RegTextHWID As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents getNameApp As System.Windows.Forms.Label
    Friend WithEvents WaitTimer As System.Windows.Forms.Timer
    Friend WithEvents RegLabelCountDown As System.Windows.Forms.Label

End Class
