﻿Public Class AppRegister
    '--------------- Deklarasi ---------------'
    Dim KeyRegis As String
    Dim CountTime As Integer = 0
#Region "Place Holder.."
    Private Sub TextBox1_MouseClick(sender As Object, e As MouseEventArgs) Handles RegText.MouseClick
        If RegText.Text = "Insert Key Register.." Then
            RegText.Text = ""
            RegText.ForeColor = Color.FloralWhite
        End If
    End Sub

    Private Sub TextBox1_MouseLeave(sender As Object, e As EventArgs) Handles RegText.MouseLeave
        If RegText.Text = "" Then
            RegText.Text = "Insert Key Register.."
            RegText.ForeColor = Color.Gainsboro
        End If
    End Sub

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles RegText.KeyPress
        If RegText.Text = "Insert Key Register.." Then
            RegText.Text = ""
            RegText.ForeColor = Color.FloralWhite
        End If
    End Sub
#End Region
#Region "Contact"
    Private Sub LinkLabel1_LinkClicked(
                                      sender As Object, e As LinkLabelLinkClickedEventArgs
                                      ) Handles LinkLabel1.LinkClicked
        If MessageBox.Show("Hubungi Pembuat..?", "~Visit Dev~",
                           MessageBoxButtons.OKCancel, MessageBoxIcon.Question
                           ) = Windows.Forms.DialogResult.OK Then
            Try
                Process.Start("https://www.facebook.com/poetralesana")
            Catch ex As Exception
                Process.Start("iexplore.exe", "https://www.facebook.com/poetralesana")
            End Try
        End If
    End Sub
#End Region
#Region "Menunggu Masuk Form Berikutnya"
    Private Sub WaitTimer_Tick(sender As Object, e As EventArgs) Handles WaitTimer.Tick
        RegLabelCountDown.Text = "Please Wait : " & CountTime
        If CountTime = -1 Then
            WaitTimer.Stop()
            CountTime = 0
            RegLabelCountDown.Visible = False

            'Insert Key 
            SetKeyRegistry(KeyRegis)

            'NextForm
            LoadingForm.Show()
            Me.Close()
        End If
        CountTime = CountTime - 1
    End Sub
#End Region
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Form1_Click(sender As Object, e As EventArgs) Handles Me.Click
        RegText.Focus()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        getNameApp.Text = "Form Register Aplikasi Bell" ' Header
        RegTextHWID.Text = GetCpuID() & getMacAddress() & mB() 'CPUID, Mac, Motherboard
        KeyRegis = Somplak(RegTextHWID.Text) 'Encrypt MachineID {Hiden Value}
        ' -===== Pengecekan Key Diregistry =====-
        If GetKeyRegistry(KeyRegis) = KeyRegis Then
            LoadingForm.Show()
            Me.Close()
        End If
        ' -=====================================-

        ' Pengaturan timer
        Timer1.Interval = 40
        WaitTimer.Interval = 1000
    End Sub
    Private Sub RegBtn_Click(sender As Object, e As EventArgs) Handles RegBtn.Click
        RegBtn.Enabled = False
        RegText.Enabled = False
        RegBtn.Text = "Please Wait..."
        Me.Cursor = Cursors.WaitCursor
        RegMsgKey.Visible = False

        Timer1.Start()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        CountTime += 1
        If CountTime = 100 Then
            Timer1.Stop()
            RegBtn.Text = "Check"
            RegBtn.Enabled = True
            RegText.Enabled = True
            CountTime = 0
            RegMsgKey.Visible = True
            Me.Cursor = Cursors.Default

            If KeyRegis = RegText.Text Then
                RegMsgKey.ForeColor = Color.LawnGreen
                RegMsgKey.Text = "Success : Key Valid" ' masuk app
                RegLabelCountDown.Visible = True
                CountTime = 5
                WaitTimer.Start()
                RegBtn.Enabled = False
                RegText.Enabled = False
                My.Computer.Audio.Play(My.Resources.Success, AudioPlayMode.Background) ' Play Sound
            Else
                RegMsgKey.ForeColor = Color.Red
                RegMsgKey.Text = "Error : Key Invalid"
                My.Computer.Audio.Play(My.Resources.Critical, AudioPlayMode.Background) ' Play Sound
            End If
        End If
    End Sub

    Private Sub Form1_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.TopMost = True
    End Sub

End Class