﻿Imports MySql.Data.MySqlClient
Public Class FormAturSekolah

    Private Sub FormAturSekolah_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Me.TopMost = True
    End Sub

    Private Sub FormAturSekolah_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        With Main
            .btn_PengNmSkolah.Enabled = True
            .btn_PengNmSkolah.Select()
        End With
    End Sub
    Private Sub FormAturSekolah_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        GetNamaANDLogoSekolah() ' Fungsi Ini Terletak Di Pengaturan Sekolah
        TextBox1.Text = AMBILNAMASEKOLAH
        TextBox2.Text = AMBILJALANSEKOLAH
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Button1.Enabled = False
        If TextBox1.Text = String.Empty Or TextBox2.Text = String.Empty Then
            MsgBox("Data Yang Anda Isi Tidak Lengkap" & vbNewLine &
                   "Silahkan Lengkapi Data & Isi Text Yang Kosong", MsgBoxStyle.Exclamation, "~~~Ditolak~~")
        ElseIf TextBox1.Text = Main.LbL_NamaSkol.Text And TextBox2.Text = Main.Lbl_Jl.Text Then
            MsgBox("Gagal melakukan perubahan", MsgBoxStyle.Exclamation, "Nama Telah Ada.")
        Else
            AMBILNAMASEKOLAH = TextBox1.Text
            AMBILJALANSEKOLAH = TextBox2.Text
            UpdateNamaSekolah()

            With Main
                If KONDISIVALUE = True Then
                    .LbL_NamaSkol.Text = AMBILNAMASEKOLAH.ToUpper
                    .Lbl_Jl.Text = StrConv(AMBILJALANSEKOLAH,
                                                VbStrConv.ProperCase)
                End If
            End With
        End If
        Button1.Enabled = True
    End Sub
End Class