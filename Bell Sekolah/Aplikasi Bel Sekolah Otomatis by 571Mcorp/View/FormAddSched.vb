﻿Imports MySql.Data.MySqlClient
Imports System.Text.RegularExpressions
Imports System.IO
Public Class FormAddSched
    Dim WAKTU As Integer
    Dim MENIT As Integer = 0
    Dim GET_ID As Integer
    Dim CheckinGData As Boolean = False
    Private Sub FormAddSched_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Me.TopMost = True
    End Sub

    Private Sub FormAddSched_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        'aktifkan "button baru" pada form1
        With Main
            If .btnBaru.Enabled = False Then
                .btnBaru.Enabled = True
            End If
            If .Button1.Enabled = False Then
                .Button1.Enabled = True
            End If
            IDSTRING = String.Empty 'Kosongkan Value ID
            .Datagrid_Jadwal.ClearSelection()
            .Btn_CopyData.Enabled = True
        End With
        StopAudioPlay()
        ParameterCopyFile = String.Empty
    End Sub
    Private Sub FormAddSched_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '//////////////// LOOP  WAKTU ////////////////
        'Loop Waktu
        While WAKTU < 12
            WAKTU += 1
            cmb_waktu.Items.Add("Jam " & WAKTU)
        End While

        'Loop Menit
        While MENIT < 60
            Dim x = cmb_minutes.Items.Add("Menit " & MENIT.ToString("D2"))
            MENIT += 1
        End While
        '////////////////////////////////////////////

        'Pengaturan Index
        cmb_waktu.SelectedIndex = 6 ' jam 7
        cmb_minutes.SelectedIndex = 0 'menit 00
        cmb_pagimalam.SelectedIndex = 0 'pagi

        AMBIL_ID() 'ambil ID
        GetSound() 'ambil file dari dir Sound
        cmb_nada.SelectedIndex = 0

        If KONDISIVALUE = True Then
            AMBIL(IDJADWAL_Label, DesctTxt, cmb_waktu, cmb_minutes, cmb_pagimalam, Gb_Hari, cmb_nada)
            If ParameterCopyFile = "IM BUTTON COPY" Then
                AMBIL_ID()
            End If
        End If
        KONDISIVALUE = False
    End Sub
    Private Function AMBIL(ByVal ID As Label, ByVal A As TextBox, ByVal CMB1 As ComboBox,
                           ByVal CMB2 As ComboBox, ByVal CMB3 As ComboBox,
                           ByVal GBHARI As GroupBox, ByVal CMB4 As ComboBox)
        Dim RbV As RadioButton
        Dim NameItem As String = ""
        Try
            conn.Open()
            query = "SELECT * FROM `jadwal` WHERE id_jadwal='" & IDSTRING & "'"
            cmnd = New MySqlCommand(query, conn)
            readDT = cmnd.ExecuteReader
            With readDT
                .Read()
                If readDT.HasRows = True Then
                    ID.Text = .Item("id_jadwal")
                    A.Text = .Item("deskripsi")

                    'Get Waktu
                    CMB1.Text = .Item("jam")
                    CMB2.Text = .Item("menit")
                    CMB3.Text = .Item("pmam")
                    NameItem = .Item("hari")

                    'Get Nada
                    CMB4.Text = .Item("nama_nada")

                    'get hari
                    For Each RbV In GBHARI.Controls
                        If RbV.Text = NameItem Then
                            RbV.Checked = True
                        End If
                    Next
                End If
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "")
            Application.Exit()
        Finally
            readDT.Close()
            conn.Close()
        End Try
        Return True
    End Function
    Public Function EscP(ByVal mystr As String)
        Return mystr.Replace("`", "``").Replace("'", "''")
    End Function
    Private Sub TambahJadwalBtn(sender As Object, e As EventArgs) Handles Btn_TambahJ.Click
        Btn_TambahJ.Enabled = False
        Dim GetValueRB As String = String.Empty
        For Each RadioBtnText As RadioButton In Gb_Hari.Controls
            If RadioBtnText.Checked = True Then
                GetValueRB = RadioBtnText.Text
            End If
        Next
        If GetValueRB = "" Or DesctTxt.Text = "" Then '//Pengecekan Text Kosong
            MsgBox("Data Yang Anda Masukan Tidak Lengkap!" & vbNewLine &
                   "Silahkan Isi Semua Data Yang Kosong.", MsgBoxStyle.Exclamation, "-=Prosses Ditolak=-")
        Else
            If btn_pilih.Enabled = True Then    'Pengecekan Pemilihan Nada
                MsgBox("Silahkan Pilih Nada Yang Ingin Digunakan", MsgBoxStyle.Exclamation, "")
            Else
                '/////////////////////////////////// UBAH DATA ///////////////////////////////////
                '>>>>>>> karena terlalu Banyak nantinya
                'jadi fungsi & prossenya saya letakan di Module "Prosses2"
                If Btn_TambahJ.Text = "Ubah Data" Then
                    Prosses2.UbahData(DesctTxt,
                                      cmb_waktu, cmb_minutes, cmb_pagimalam,
                                      Gb_Hari, cmb_nada, IDJADWAL_Label, btn_pilih)
                Else
                    '//////////////////////////////////////////////////////////////////////////////////

                    '/////////////////////////////////// Prosses Insert Data ///////////////////////////////////
                    Dim Q = MessageBox.Show(
                            "Deskripsi = " & DesctTxt.Text & vbNewLine &
                            "Waktu = " & cmb_waktu.Text & " - " & cmb_minutes.Text & " - " & cmb_pagimalam.Text & vbNewLine &
                            "Hari = " & GetValueRB & vbNewLine &
                            "Nada Bell = " & cmb_nada.Text, "Tambahkan Jadwal...?", MessageBoxButtons.OKCancel,
                            MessageBoxIcon.Question)

                    If Q = Windows.Forms.DialogResult.OK Then
                        Try
                            conn.Open()
                            query = "INSERT INTO `jadwal`(`id_jadwal`, `deskripsi`, `jam`, `menit`, `pmam`, `hari`, `nama_nada`) VALUES ('" &
                               IDJADWAL_Label.Text & "','" & EscP(DesctTxt.Text) & "','" &
                                cmb_waktu.Text & "','" & cmb_minutes.Text & "','" &
                                cmb_pagimalam.Text & "','" & GetValueRB.ToString & "','" & EscP(cmb_nada.Text) & "')"
                            cmnd = New MySqlCommand(query, conn)
                            cmnd.ExecuteReader()
                            CheckinGData = True
                            MsgBox("Jadwal Pada Hari " & GetValueRB & " Berhasil Ditambahkan..", MsgBoxStyle.Information, "")
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                            If CheckinGData = True Then
                                Label_StatuSNada.Text = "Nada : -"
                                AMBIL_ID()
                                CheckinGData = False
                                Main.TampilkanData()
                                EXTRACT_DATA.TampilkanDataExtract()
                                btn_pilih.Enabled = True
                            End If
                        End Try
                    End If
                End If
            End If
        End If
        Btn_TambahJ.Enabled = True
    End Sub
    Public Sub AMBIL_ID() ' Ambil ID Jadwal
        Try
            conn.Open()
            query = "SELECT * FROM `jadwal` WHERE `id_jadwal` in (SELECT MAX(id_jadwal) FROM `jadwal`)"
            cmnd = New MySql.Data.MySqlClient.MySqlCommand(query, conn)
            readDT = cmnd.
                        ExecuteReader
            readDT.Read()
            If readDT.HasRows Then
                GET_ID = Microsoft.VisualBasic.Mid(readDT.GetString(
                                                  "id_jadwal"), 5) ' Ambil Value
                GET_ID += 1 ' Hitung 1
                IDJADWAL_Label.Text = "JDW-00" & GET_ID.ToString("D2") '00
            Else
                IDJADWAL_Label.Text = "JDW-0001"
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            readDT.Close()
            conn.Close()
        End Try
    End Sub
    Private Sub GetSound()
        Dim GetFileWav As String() = Directory.GetFiles(
        Path.GetFullPath(PathFile), "*.wav", SearchOption.TopDirectoryOnly)
        For Each GetAllExtension In GetFileWav
            cmb_nada.Items.Add(GetAllExtension.Replace(PathFile & "\", ""))
        Next
    End Sub

    Private Sub btnPlay_Click(sender As Object, e As EventArgs) Handles btnPlay.Click
        'Play Sound
        PlayAudio()
    End Sub
    Private Sub PlayAudio()
        My.Computer.Audio.Play(PathFile & "\" & cmb_nada.Text, AudioPlayMode.Background)
    End Sub
 
    Private Sub btn_pilih_Click(sender As Object, e As EventArgs) Handles btn_pilih.Click
        Dim q = MessageBox.Show("Memilih Nada " & vbNewLine & cmb_nada.Text & " Sebagai nada dering..?", "Pilih Nada...?",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If q = MsgBoxResult.Yes Then
            MsgBox("Berhasil Memilih Nada " & cmb_nada.Text, MsgBoxStyle.Information, "")
            Label_StatuSNada.Text = "Nada : " & cmb_nada.Text
            btn_pilih.Enabled = False
        End If
    End Sub

    Private Sub cmb_nada_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmb_nada.SelectedValueChanged
        StopAudioPlay()
        If btn_pilih.Enabled = False Then
            btn_pilih.Enabled = True
        End If
        Label_StatuSNada.Text = "Nada : -"
    End Sub
    Private Sub cmb_nada_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmb_nada.KeyPress
        e.Handled = True
    End Sub

    Private Sub cmb_waktu_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmb_waktu.KeyPress
        e.Handled = True
    End Sub

    Private Sub cmb_minutes_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmb_minutes.KeyPress
        e.Handled = True
    End Sub

    Private Sub cmb_pagimalam_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmb_pagimalam.KeyPress
        e.Handled = True
    End Sub
End Class