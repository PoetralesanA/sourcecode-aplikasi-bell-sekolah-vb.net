﻿Public Class b_dBs
    Dim path_B
    Private Sub LoadDt() Handles MyBase.Load
        Timer1.Start()
        Try
            Dim LogPath() = System.IO.File.
                            ReadAllLines(Application.StartupPath & "\App Backup.log")
            ListBox1.Items.AddRange(LogPath)
        Catch x As Exception
        End Try
        MetroRadioButton1.Checked = True
    End Sub
    Private Sub LoadDTClose() Handles MyBase.FormClosed
        Try

            Dim Write As New System.IO.
                            StreamWriter(Application.StartupPath & "\App Backup.log")

            For Each listBx In ListBox1.Items
                Write.WriteLine(listBx)
            Next
            Write.Close()
        Catch x As Exception

        End Try
        With Main
            .btn_backup.Enabled = True
        End With
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If MetroRadioButton1.Checked = True Then
            SaveFileDialog1.FileName = "db_bellsekolah.sql"
            SaveFileDialog1.Filter = "|*.sql"
            SaveFileDialog1.ShowDialog()
        Else
            Try
                Dim a = MetroFramework.MetroMessageBox.Show(Me,
                                                        "Ingin mengapus Semua Log?", "",
                                                                MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
                If a = Windows.Forms.DialogResult.Yes Then
                    ListBox1.Items.Clear()
                    MetroFramework.MetroMessageBox.Show(Me, "Semua Log Berhasil Di Hapus..", "",
                                                                MessageBoxButtons.OK, MessageBoxIcon.Information)

                    FileIO.FileSystem.DeleteFile(Application.
                                                 StartupPath & "\App Backup.log")
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If
    End Sub
    Private Sub SaveFileDialog1_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles SaveFileDialog1.FileOk
        Dim Checkname As String
        Checkname = System.IO.Path.GetFileName(SaveFileDialog1.FileName)

        If Checkname = "db_bellsekolah.sql" Or Checkname = "db_bellsekolah" Then
            TimerPgB.Start()
            Button1.Enabled = False
        Else
            e.Cancel = True
            MsgBox("Gagal menyimpan!" & vbNewLine & "Gunakan nama default db_bellsekolah atau db_bellsekolah.sql", MsgBoxStyle.Exclamation, "")
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles TimerPgB.Tick
        With ProgressBar1
            .Value += 1
            If .Value = 100 Then
                TimerPgB.Stop()

                Button1.Enabled = True ' Btn Kembali Dapat Digunakan

                path_B = System.IO.Path.GetFullPath(SaveFileDialog1.FileName)

                Try
                    conn.Open()

                    Dim mysqld_B As New MySql.Data.MySqlClient.
                                        MySqlBackup(cmnd)

                    cmnd.Connection = conn
                    mysqld_B.ExportToFile(path_B)

                    With ListBox1
                        .Items.Add(">> " & Format(Now(), "MMM-dd-yyyy-h:mm:tt") & " Backup berhasil")
                        .Items.Add("      Lokasi " & path_B)
                        MsgBox("==Backup Created==" & vbNewLine &
                                              Format(Now(), "MMM-dd-yyyy-h:mm:tt"), MsgBoxStyle.Information, "")

                        ProgressBar1.Value = 0
                    End With
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Critical, "")
                    ListBox1.Items.Add(">> " & Format(Now(), "MMM-dd-yyyy-h:mm:tt"))
                    ListBox1.Items.Add("      #Gagal terhubung ke server")
                    .Value = 0
                Finally
                    conn.Close()
                End Try
            End If

        End With
    End Sub

    Private Sub Timer1_Tick_1(sender As Object, e As EventArgs) Handles Timer1.Tick
        Me.Opacity += 0.1
        If Me.Opacity = 100 Then
            Timer1.Stop()
        End If
    End Sub


    Private Sub b_dBs_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.TopMost = True
    End Sub
End Class