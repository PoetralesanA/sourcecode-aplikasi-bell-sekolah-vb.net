﻿Public Class FormUbahLogo
    Dim path As String
    Private Sub FormUbahLogo_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        With Main
            .Button4.Enabled = True
            .Select()
        End With
    End Sub
    Private Sub CheckImg()
        If AMBILNAMA_LOGO <> "" Then
            PictureBox1.ImageLocation = Application.StartupPath & "\img\" & AMBILNAMA_LOGO
            Label2.Text = "Name Logo : " & AMBILNAMA_LOGO
        Else
            PictureBox1.Image = My.Resources.NoIMG
        End If
    End Sub
    Private Sub FormUbahLogo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Button1.Cursor = Cursors.Hand
        ValidasiLogoExistOrNo()
        CheckImg()
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        KONDISIVALUE = False
        If Button1.Text = "Ubah Logo" Then
            Dim Q = MessageBox.Show("Ingin Merubah Logo..?", "",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If Q = Windows.Forms.DialogResult.Yes Then
                AMBILNAMA_LOGO = System.IO.Path.GetFileName(OpenFileDialog1.FileName)
                Try
                    conn.Open()
                    query = "UPDATE `sekolah` SET `logo`='" & AMBILNAMA_LOGO & "' WHERE `id`=1"
                    cmnd = New MySql.Data.MySqlClient.MySqlCommand(query, conn)
                    cmnd.ExecuteReader()
                    KONDISIVALUE = True
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Critical, "")
                Finally
                    conn.Close()
                    If KONDISIVALUE = True Then
                        Button1.Text = "Cari Logo>>"
                        MsgBox("Logo Berhasil Diubah...", MsgBoxStyle.Information, "")

                        'Copy Logo Ke Folder /img
                        Try
                            System.IO.File.Copy(path, Application.StartupPath & "\img\" & AMBILNAMA_LOGO, True)
                        Catch ex As Exception
                            MsgBox(ex.Message, MsgBoxStyle.Critical, "")
                            Application.Exit()
                        End Try
                        ValidasiLogoExistOrNo()
                    End If
                End Try
            Else
                Button1.Text = "Cari Logo>>" ' jika Msgbx cencel ubah kecarilogo
                Label2.Text = "Name Logo : " & AMBILNAMA_LOGO
                CheckImg()
            End If
        Else
            If Button1.Text = "Cari Logo>>" Then ' Button text cari Logo? Browse File!!
                'Browse File.....
                With OpenFileDialog1
                    .Filter = "Chose Image Files (*.PNG) | *.png"
                    .FileName = ""
                    .Title = "Cari Logo Anda"
                    .ShowDialog()
                End With
            End If
        End If

    End Sub

    Private Sub OpenFileDialog1_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        'GET VALUE
        path = System.IO.Path.GetFullPath(OpenFileDialog1.FileName) ' ambil full path
        'CONTROL  AND GET VAL
        Button1.Text = "Ubah Logo"
        Label2.Text = "Name Logo : " & System.IO.Path.GetFileName(OpenFileDialog1.FileName)
        PictureBox1.ImageLocation = path ' ubah img
    End Sub
End Class