﻿Imports System.Text.RegularExpressions
Imports MySql.Data.MySqlClient
Public Class EXTRACT_DATA
    Dim PARAMETERCLICK As Boolean
    Private Sub EXTRACT_DATA_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If v_CheckStatrup = True Then
            e.Cancel = True
        Else
            e.Cancel = False
        End If
    End Sub
    Public Sub TampilkanDataExtract()
        Try
            Dim dt_table As New DataTable
            conn.Open()
            '//Full select
            'query = "Select id_buku as 'ID Buku', judul as Judul, pengarang as Pengarang, " &
            '        "penerbit as Penerbit, sinopsis as Sinopsis, stock as Stock, jenis as Jenis from t_buku"
            query = "Select * from jadwal where hari='" & conv_tanggal(Format(CDate(Now.Date), "dddd")) & "'"
            cmnd = New MySql.Data.MySqlClient.MySqlCommand(query, conn)
            dt_Adptr.SelectCommand = cmnd
            Dim dtTable As New DataTable
            dt_Adptr.Fill(dtTable)
            With DataGridView1
                .DataSource = dtTable 'GetSource
                .Columns(0).HeaderText = "ID"
                .Columns(1).HeaderText = "Deskripsi"
                .Columns(2).HeaderText = "Jam Bell"
                .Columns(3).HeaderText = "Menit"
                .Columns(4).HeaderText = "Pm/Am"
                .Columns(5).HeaderText = "Hari"
                .Columns(6).HeaderText = "Nada Bell"
            End With
        Catch ex As Exception
            MessageBox.Show("Error !!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            conn.Close()
            ExtrcJam_Btn.PerformClick()

            'kondisi startup form
            If v_CheckStatrup = False Then
                MsgBox("Startup Harus Ke Form Main", MsgBoxStyle.Critical, "")
                Application.Exit()
            End If
        End Try
    End Sub
    Private Sub LoadD() Handles MyBase.Load
        TampilkanDataExtract()
        CheckTime.Start()
    End Sub
    Private Sub EXTRACTJAM()
        For i = 0 To DataGridView1.RowCount - 2
            Dim GetID_Jadwal = DataGridView1.Rows(i).Cells(0).Value
            Dim CellJam = DataGridView1.Rows(i).Cells(2).Value
            Dim CellMenit = DataGridView1.Rows(i).Cells(3).Value
            Dim CellPMAM = DataGridView1.Rows(i).Cells(4).Value
            '!!FIND NUMBER & EXTRACT!!
            Dim GetStringJam As String = CellJam
            Dim GetStringMenit As String = CellMenit
            Dim arr1() As String = GetStringJam.Split("-")
            Dim arr2() As String = CellMenit.Split("-")
            CellJam = Regex.Replace(arr1(0), "[^0-9]", "")
            CellMenit = Regex.Replace(arr2(0), "[^0-9]", "")
            ListBox1.Items.Add(CellJam & ":" & CellMenit & " " & CellPMAM)
            ListBox2.Items.Add(GetID_Jadwal)
        Next

    End Sub
    Public Sub Button1_Click(sender As Object, e As EventArgs) Handles ExtrcJam_Btn.Click
        ListBox1.Items.Clear()
        ListBox2.Items.Clear()
        EXTRACTJAM()
    End Sub
    Private Sub CheckTime_Tick(sender As Object, e As EventArgs) Handles CheckTime.Tick
        For indx As Integer = 0 To ListBox1.Items.Count - 1
            If ListBox1.Items(indx).ToString.Contains(Label1.Text) Then
                ListBox1.SelectedIndex = indx
                ListBox2.SelectedIndex = indx
                Exit For
            End If
        Next
    End Sub
    Private Sub ListBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox2.SelectedIndexChanged
        If PARAMETERCLICK = True Then
        Else
            Try
                conn.Open()
                query = "SELECT `id_jadwal`, nama_nada FROM `jadwal` WHERE id_jadwal='" & ListBox2.SelectedItem & "'"
                cmnd = New MySqlCommand(query, conn)
                readDT = cmnd.ExecuteReader
                While readDT.Read
                    Label2.Text = readDT.Item("nama_nada").ToString()
                End While
                My.Computer.Audio.Play(PathFile & "\" & Label2.Text, AudioPlayMode.Background)
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "")
                Application.Exit()
            Finally
                conn.Close()
            End Try
        End If
        PARAMETERCLICK = False
    End Sub

    Private Sub ListBox2_MouseClick(sender As Object, e As MouseEventArgs) Handles ListBox2.MouseClick
        PARAMETERCLICK = True ' HANDLE CLICK & PLAY SOUND
    End Sub
End Class