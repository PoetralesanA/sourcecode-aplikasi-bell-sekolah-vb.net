﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAddSched
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormAddSched))
        Me.DesctTxt = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmb_waktu = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmb_minutes = New System.Windows.Forms.ComboBox()
        Me.Gb_Hari = New System.Windows.Forms.GroupBox()
        Me.Rb_Mggu = New System.Windows.Forms.RadioButton()
        Me.Rb_Sab = New System.Windows.Forms.RadioButton()
        Me.Rb_Jum = New System.Windows.Forms.RadioButton()
        Me.Rb_kam = New System.Windows.Forms.RadioButton()
        Me.Rb_Rab = New System.Windows.Forms.RadioButton()
        Me.Rb_Sel = New System.Windows.Forms.RadioButton()
        Me.Rb_Sen = New System.Windows.Forms.RadioButton()
        Me.Btn_TambahJ = New System.Windows.Forms.Button()
        Me.Gb_Sched = New System.Windows.Forms.GroupBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.IDJADWAL_Label = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmb_pagimalam = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label_StatuSNada = New System.Windows.Forms.Label()
        Me.btn_pilih = New System.Windows.Forms.Button()
        Me.btnPlay = New System.Windows.Forms.Button()
        Me.cmb_nada = New System.Windows.Forms.ComboBox()
        Me.Gb_Hari.SuspendLayout()
        Me.Gb_Sched.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DesctTxt
        '
        Me.DesctTxt.Location = New System.Drawing.Point(120, 69)
        Me.DesctTxt.Name = "DesctTxt"
        Me.DesctTxt.Size = New System.Drawing.Size(176, 25)
        Me.DesctTxt.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(29, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 22)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Description :"
        Me.Label1.UseCompatibleTextRendering = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(29, 108)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 22)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Set Timer :"
        Me.Label2.UseCompatibleTextRendering = True
        '
        'cmb_waktu
        '
        Me.cmb_waktu.FormattingEnabled = True
        Me.cmb_waktu.Location = New System.Drawing.Point(120, 107)
        Me.cmb_waktu.Name = "cmb_waktu"
        Me.cmb_waktu.Size = New System.Drawing.Size(63, 25)
        Me.cmb_waktu.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(187, 110)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(11, 17)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = ":"
        '
        'cmb_minutes
        '
        Me.cmb_minutes.FormattingEnabled = True
        Me.cmb_minutes.Location = New System.Drawing.Point(199, 107)
        Me.cmb_minutes.Name = "cmb_minutes"
        Me.cmb_minutes.Size = New System.Drawing.Size(79, 25)
        Me.cmb_minutes.TabIndex = 7
        '
        'Gb_Hari
        '
        Me.Gb_Hari.BackColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.Gb_Hari.Controls.Add(Me.Rb_Mggu)
        Me.Gb_Hari.Controls.Add(Me.Rb_Sab)
        Me.Gb_Hari.Controls.Add(Me.Rb_Jum)
        Me.Gb_Hari.Controls.Add(Me.Rb_kam)
        Me.Gb_Hari.Controls.Add(Me.Rb_Rab)
        Me.Gb_Hari.Controls.Add(Me.Rb_Sel)
        Me.Gb_Hari.Controls.Add(Me.Rb_Sen)
        Me.Gb_Hari.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Gb_Hari.ForeColor = System.Drawing.Color.Honeydew
        Me.Gb_Hari.Location = New System.Drawing.Point(49, 156)
        Me.Gb_Hari.Name = "Gb_Hari"
        Me.Gb_Hari.Size = New System.Drawing.Size(295, 83)
        Me.Gb_Hari.TabIndex = 16
        Me.Gb_Hari.TabStop = False
        Me.Gb_Hari.Text = "Jadwal Alarm/Hari"
        '
        'Rb_Mggu
        '
        Me.Rb_Mggu.AutoSize = True
        Me.Rb_Mggu.Location = New System.Drawing.Point(153, 52)
        Me.Rb_Mggu.Name = "Rb_Mggu"
        Me.Rb_Mggu.Size = New System.Drawing.Size(73, 21)
        Me.Rb_Mggu.TabIndex = 15
        Me.Rb_Mggu.TabStop = True
        Me.Rb_Mggu.Text = "Minggu"
        Me.Rb_Mggu.UseVisualStyleBackColor = True
        '
        'Rb_Sab
        '
        Me.Rb_Sab.AutoSize = True
        Me.Rb_Sab.Location = New System.Drawing.Point(84, 53)
        Me.Rb_Sab.Name = "Rb_Sab"
        Me.Rb_Sab.Size = New System.Drawing.Size(61, 21)
        Me.Rb_Sab.TabIndex = 14
        Me.Rb_Sab.TabStop = True
        Me.Rb_Sab.Text = "Sabtu"
        Me.Rb_Sab.UseVisualStyleBackColor = True
        '
        'Rb_Jum
        '
        Me.Rb_Jum.AutoSize = True
        Me.Rb_Jum.Location = New System.Drawing.Point(19, 53)
        Me.Rb_Jum.Name = "Rb_Jum"
        Me.Rb_Jum.Size = New System.Drawing.Size(63, 21)
        Me.Rb_Jum.TabIndex = 13
        Me.Rb_Jum.TabStop = True
        Me.Rb_Jum.Text = "Jumat"
        Me.Rb_Jum.UseVisualStyleBackColor = True
        '
        'Rb_kam
        '
        Me.Rb_kam.AutoSize = True
        Me.Rb_kam.Location = New System.Drawing.Point(214, 26)
        Me.Rb_kam.Name = "Rb_kam"
        Me.Rb_kam.Size = New System.Drawing.Size(62, 21)
        Me.Rb_kam.TabIndex = 12
        Me.Rb_kam.TabStop = True
        Me.Rb_kam.Text = "Kamis"
        Me.Rb_kam.UseVisualStyleBackColor = True
        '
        'Rb_Rab
        '
        Me.Rb_Rab.AutoSize = True
        Me.Rb_Rab.Location = New System.Drawing.Point(153, 25)
        Me.Rb_Rab.Name = "Rb_Rab"
        Me.Rb_Rab.Size = New System.Drawing.Size(57, 21)
        Me.Rb_Rab.TabIndex = 11
        Me.Rb_Rab.TabStop = True
        Me.Rb_Rab.Text = "Rabu"
        Me.Rb_Rab.UseVisualStyleBackColor = True
        '
        'Rb_Sel
        '
        Me.Rb_Sel.AutoSize = True
        Me.Rb_Sel.Location = New System.Drawing.Point(84, 24)
        Me.Rb_Sel.Name = "Rb_Sel"
        Me.Rb_Sel.Size = New System.Drawing.Size(63, 21)
        Me.Rb_Sel.TabIndex = 10
        Me.Rb_Sel.TabStop = True
        Me.Rb_Sel.Text = "Selasa"
        Me.Rb_Sel.UseVisualStyleBackColor = True
        '
        'Rb_Sen
        '
        Me.Rb_Sen.AutoSize = True
        Me.Rb_Sen.Location = New System.Drawing.Point(19, 24)
        Me.Rb_Sen.Name = "Rb_Sen"
        Me.Rb_Sen.Size = New System.Drawing.Size(59, 21)
        Me.Rb_Sen.TabIndex = 9
        Me.Rb_Sen.TabStop = True
        Me.Rb_Sen.Text = "Senin"
        Me.Rb_Sen.UseVisualStyleBackColor = True
        '
        'Btn_TambahJ
        '
        Me.Btn_TambahJ.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_TambahJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Btn_TambahJ.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_TambahJ.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Btn_TambahJ.Location = New System.Drawing.Point(29, 338)
        Me.Btn_TambahJ.Name = "Btn_TambahJ"
        Me.Btn_TambahJ.Size = New System.Drawing.Size(329, 50)
        Me.Btn_TambahJ.TabIndex = 17
        Me.Btn_TambahJ.Text = "Tambahkan>>"
        Me.Btn_TambahJ.UseVisualStyleBackColor = True
        '
        'Gb_Sched
        '
        Me.Gb_Sched.Controls.Add(Me.Panel2)
        Me.Gb_Sched.Controls.Add(Me.cmb_pagimalam)
        Me.Gb_Sched.Controls.Add(Me.Panel1)
        Me.Gb_Sched.Controls.Add(Me.Btn_TambahJ)
        Me.Gb_Sched.Controls.Add(Me.Gb_Hari)
        Me.Gb_Sched.Controls.Add(Me.cmb_minutes)
        Me.Gb_Sched.Controls.Add(Me.Label3)
        Me.Gb_Sched.Controls.Add(Me.cmb_waktu)
        Me.Gb_Sched.Controls.Add(Me.Label2)
        Me.Gb_Sched.Controls.Add(Me.Label1)
        Me.Gb_Sched.Controls.Add(Me.DesctTxt)
        Me.Gb_Sched.Controls.Add(Me.GroupBox1)
        Me.Gb_Sched.ForeColor = System.Drawing.SystemColors.Control
        Me.Gb_Sched.Location = New System.Drawing.Point(11, 12)
        Me.Gb_Sched.Name = "Gb_Sched"
        Me.Gb_Sched.Size = New System.Drawing.Size(393, 407)
        Me.Gb_Sched.TabIndex = 18
        Me.Gb_Sched.TabStop = False
        Me.Gb_Sched.Text = "Schedule"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(56, Byte), Integer), CType(CType(56, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.Panel2.Controls.Add(Me.IDJADWAL_Label)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Location = New System.Drawing.Point(23, 21)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(347, 32)
        Me.Panel2.TabIndex = 26
        '
        'IDJADWAL_Label
        '
        Me.IDJADWAL_Label.AutoSize = True
        Me.IDJADWAL_Label.BackColor = System.Drawing.Color.FromArgb(CType(CType(56, Byte), Integer), CType(CType(56, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.IDJADWAL_Label.Font = New System.Drawing.Font("Segoe UI", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IDJADWAL_Label.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.IDJADWAL_Label.Location = New System.Drawing.Point(94, 6)
        Me.IDJADWAL_Label.Name = "IDJADWAL_Label"
        Me.IDJADWAL_Label.Size = New System.Drawing.Size(58, 17)
        Me.IDJADWAL_Label.TabIndex = 25
        Me.IDJADWAL_Label.Text = "JDW000"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label6.Location = New System.Drawing.Point(9, 6)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 17)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "ID Jadwal : "
        '
        'cmb_pagimalam
        '
        Me.cmb_pagimalam.FormattingEnabled = True
        Me.cmb_pagimalam.Items.AddRange(New Object() {"PM", "AM"})
        Me.cmb_pagimalam.Location = New System.Drawing.Point(294, 107)
        Me.cmb_pagimalam.Name = "cmb_pagimalam"
        Me.cmb_pagimalam.Size = New System.Drawing.Size(68, 25)
        Me.cmb_pagimalam.TabIndex = 19
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel1.Location = New System.Drawing.Point(23, 144)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(347, 1)
        Me.Panel1.TabIndex = 18
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label_StatuSNada)
        Me.GroupBox1.Controls.Add(Me.btn_pilih)
        Me.GroupBox1.Controls.Add(Me.btnPlay)
        Me.GroupBox1.Controls.Add(Me.cmb_nada)
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.GroupBox1.Location = New System.Drawing.Point(6, 236)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(381, 96)
        Me.GroupBox1.TabIndex = 23
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Pilih Nada"
        '
        'Label_StatuSNada
        '
        Me.Label_StatuSNada.BackColor = System.Drawing.Color.Transparent
        Me.Label_StatuSNada.Font = New System.Drawing.Font("Segoe UI", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_StatuSNada.ForeColor = System.Drawing.Color.White
        Me.Label_StatuSNada.Location = New System.Drawing.Point(7, 56)
        Me.Label_StatuSNada.Name = "Label_StatuSNada"
        Me.Label_StatuSNada.Size = New System.Drawing.Size(368, 20)
        Me.Label_StatuSNada.TabIndex = 23
        Me.Label_StatuSNada.Text = "Nada : -"
        '
        'btn_pilih
        '
        Me.btn_pilih.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_pilih.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(147, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(155, Byte), Integer))
        Me.btn_pilih.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.btn_pilih.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_pilih.Location = New System.Drawing.Point(300, 23)
        Me.btn_pilih.Name = "btn_pilih"
        Me.btn_pilih.Size = New System.Drawing.Size(75, 25)
        Me.btn_pilih.TabIndex = 22
        Me.btn_pilih.Text = "Pilih"
        Me.btn_pilih.UseVisualStyleBackColor = True
        '
        'btnPlay
        '
        Me.btnPlay.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPlay.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(147, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(155, Byte), Integer))
        Me.btnPlay.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(96, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.btnPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPlay.Location = New System.Drawing.Point(225, 23)
        Me.btnPlay.Name = "btnPlay"
        Me.btnPlay.Size = New System.Drawing.Size(75, 25)
        Me.btnPlay.TabIndex = 21
        Me.btnPlay.Text = "Play"
        Me.btnPlay.UseVisualStyleBackColor = True
        '
        'cmb_nada
        '
        Me.cmb_nada.FormattingEnabled = True
        Me.cmb_nada.Location = New System.Drawing.Point(10, 24)
        Me.cmb_nada.Name = "cmb_nada"
        Me.cmb_nada.Size = New System.Drawing.Size(209, 25)
        Me.cmb_nada.TabIndex = 20
        '
        'FormAddSched
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(14, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(31, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(415, 429)
        Me.Controls.Add(Me.Gb_Sched)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormAddSched"
        Me.Text = "Atur Jadwal"
        Me.Gb_Hari.ResumeLayout(False)
        Me.Gb_Hari.PerformLayout()
        Me.Gb_Sched.ResumeLayout(False)
        Me.Gb_Sched.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DesctTxt As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmb_waktu As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmb_minutes As System.Windows.Forms.ComboBox
    Friend WithEvents Gb_Hari As System.Windows.Forms.GroupBox
    Friend WithEvents Btn_TambahJ As System.Windows.Forms.Button
    Friend WithEvents Gb_Sched As System.Windows.Forms.GroupBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmb_pagimalam As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label_StatuSNada As System.Windows.Forms.Label
    Friend WithEvents btn_pilih As System.Windows.Forms.Button
    Friend WithEvents btnPlay As System.Windows.Forms.Button
    Friend WithEvents cmb_nada As System.Windows.Forms.ComboBox
    Friend WithEvents IDJADWAL_Label As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Rb_Mggu As System.Windows.Forms.RadioButton
    Friend WithEvents Rb_Sab As System.Windows.Forms.RadioButton
    Friend WithEvents Rb_Jum As System.Windows.Forms.RadioButton
    Friend WithEvents Rb_kam As System.Windows.Forms.RadioButton
    Friend WithEvents Rb_Rab As System.Windows.Forms.RadioButton
    Friend WithEvents Rb_Sel As System.Windows.Forms.RadioButton
    Friend WithEvents Rb_Sen As System.Windows.Forms.RadioButton
End Class
