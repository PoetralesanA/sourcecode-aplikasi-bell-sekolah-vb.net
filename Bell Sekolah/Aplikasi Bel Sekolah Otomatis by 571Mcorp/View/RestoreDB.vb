﻿Imports MySql.Data.MySqlClient
Public Class RestoreDB
    Dim CheckExistDB As Boolean = False
    Dim RestartParameter As Boolean = False
    Private Sub opf_browse_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles opf_browse.FileOk
        Txt_path.Text = opf_browse.FileName
        btn_restore.Enabled = True
    End Sub

    Private Sub btn_browse_Click(sender As Object, e As EventArgs) Handles btn_browse.Click
        opf_browse.FileName = "db_bellsekolah.sql"
        opf_browse.Filter = "|*.sql"
        opf_browse.ShowDialog()
    End Sub

    Private Sub btn_restore_Click(sender As Object, e As EventArgs) Handles btn_restore.Click
        'alur > jika db_perpus tidak tersedia -> Buat DB perpus _
        ' jika tersedia Lanjutkan Prosses Backup
        btn_restore.Enabled = False
        Dim q = MessageBox.Show("Restore Database..?", "",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If q = Windows.Forms.DialogResult.Yes Then

            Try 'restre db
                Dim RestoreConn As New MySqlConnection("server=" + txt_srv.Text + "; username=" + txt_ur.Text + "; pwd=" + txt_pwd.Text + ";") 'Koneksi Restore
                conn.Open()
                Dim db_backup As New MySqlBackup(cmnd)
                cmnd.Connection = conn

                db_backup.ImportFromFile(Txt_path.Text)
                conn.Close()
                MsgBox("Restore Sukses", MsgBoxStyle.Information, "")
                RestartParameter = True 'jika restore berhasil, maka ketika form close, Aplikasi restart
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "")
            Finally
                conn.Close()
                btn_restore.Enabled = False
            End Try
        Else
            btn_restore.Enabled = True
            MsgBox("Batal mengembalikan.", MsgBoxStyle.Exclamation, "Batal")
        End If
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim RestoreConn As New MySqlConnection("server=" + txt_srv.Text + "; username=" + txt_ur.Text + "; pwd=" + txt_pwd.Text + ";") 'Koneksi Restore
        Try
            RestoreConn.Open()
            RestoreConn.Close()

            StartCheckDB()

            MsgBox("Berhasil Terhubung..." & vbNewLine & _
                   "Silahkan browse untuk restore data", MsgBoxStyle.Information, "")
            'Matikan fungsi textboxt dan button jika berhasil terhubung
            Button1.Enabled = False
            txt_ur.Enabled = False
            txt_srv.Enabled = False
            txt_pwd.Enabled = False
            CheckBox1.Enabled = False
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            txt_pwd.UseSystemPasswordChar = False
        Else
            txt_pwd.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub RestoreDB_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.TopMost = True
    End Sub

    Private Sub RestoreDB_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Main.btn_restoreDB.Enabled = True
        If RestartParameter = True Then 'check perubahan
            MsgBox(
                    "Aplikasi akan melakukan restart untuk memperbaharui data.",
                   MsgBoxStyle.Information, "Pemberitahuan!"
                   )
            Application.Restart() 'jika berubah (true) aplikasi restart
        End If
    End Sub
#Region "Created DB or No"
    Private Sub StartCheckDB()
        ' Check Db Exist Or No
        Dim RestoreConn As New MySqlConnection("server=" + txt_srv.Text + "; username=" + txt_ur.Text + "; pwd=" + txt_pwd.Text + ";") 'Koneksi Restore
        Try
            RestoreConn.Open()
            Dim CheckDb = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='db_bellsekolah'"
            cmnd = New MySqlCommand(CheckDb, RestoreConn)
            readDT = cmnd.ExecuteReader

            If Not readDT.HasRows = True Then 'jika db perpus tidak tersedia
                RestoreConn.Close()
                CheckExistDB = False 'tidak tersedia

                If CheckExistDB = False Then 'jika tidak ada, Maka
                    Try
                        RestoreConn.Open() 'buka conn
                        Dim Q_create = "CREATE DATABASE db_bellsekolah;" ' Buat DB
                        cmnd = New MySqlCommand(Q_create, RestoreConn)
                        cmnd.ExecuteReader() 'eksekusi
                        RestoreConn.Close() 'tutup

                        btn_browse.Enabled = True
                    Catch ex As Exception
                        MsgBox(ex.Message, MsgBoxStyle.Critical, "")
                        Application.Exit()
                    End Try
                End If
            Else
                RestoreConn.Close()
                btn_browse.Enabled = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "")
        End Try
    End Sub
#End Region
End Class