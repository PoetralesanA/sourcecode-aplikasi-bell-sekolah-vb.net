﻿Public Class LoadingForm
    Dim x As Integer = 0
    Private Sub AktifForm(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.TopMost = True
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        x += 1
        If x = 100 Then
            Timer1.Stop()
            Main.Show()
            Me.Close()
        End If
    End Sub

    Private Sub LoadingForm_Load(sender As Object, e As EventArgs) Handles Me.Load
        Timer1.Start()
    End Sub
End Class