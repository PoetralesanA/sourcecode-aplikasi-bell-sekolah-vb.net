﻿Imports MySql.Data.MySqlClient
Imports System.Text.RegularExpressions
Public Class Main
    Public Sub TampilkanData()
        Try
            Dim dt_table As New DataTable
            conn.Open()
            '//Full select
            'query = "Select id_buku as 'ID Buku', judul as Judul, pengarang as Pengarang, " &
            '        "penerbit as Penerbit, sinopsis as Sinopsis, stock as Stock, jenis as Jenis from t_buku"
            query = "Select * from jadwal"
            cmnd = New MySql.Data.MySqlClient.MySqlCommand(query, conn)

            dt_Adptr.SelectCommand = cmnd
            Dim dtTable As New DataTable
            dt_Adptr.Fill(dtTable)
            With Datagrid_Jadwal
                .DataSource = dtTable 'GetSource
                .Columns(0).HeaderText = "ID"
                .Columns(1).HeaderText = "Deskripsi"
                .Columns(2).HeaderText = "Jam Bell"
                .Columns(3).HeaderText = "Menit"
                .Columns(4).HeaderText = "Pm/Am"
                .Columns(5).HeaderText = "Hari"
                .Columns(6).HeaderText = "Nada Bell"
            End With
        Catch ex As Exception
            MessageBox.Show("Error !!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            conn.Close()
            Label1.Text = "Result Count : " & Datagrid_Jadwal.RowCount
        End Try
    End Sub

    Private Sub FormMain_FormClosing(
                                    sender As Object, e As FormClosingEventArgs
                                    ) Handles Me.FormClosing
        e.Cancel = True
        Me.WindowState = FormWindowState.Minimized
    End Sub
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        version_Software.Text = "Software Version " & vbNewLine & Application.ProductVersion
        Me.Text = "Bell Sekolah Version " & Application.ProductVersion
        TimerCountAndCheck.Start()
        TampilkanData()
        v_CheckStatrup = True 'Menadakan Startup Pada Form Main
        EXTRACT_DATA.Show()

        GetNamaANDLogoSekolah() ' Mengambil Nama sekolah
        LbL_NamaSkol.Text = AMBILNAMASEKOLAH
        Lbl_Jl.Text = AMBILJALANSEKOLAH
        Datagrid_Jadwal.ClearSelection()
    End Sub
    Private Sub GetNmSchool(sender As Object, e As EventArgs) Handles LbL_NamaSkol.Click
        MsgBox("Selamat datang di " & LbL_NamaSkol.Text & " || " & Lbl_Jl.Text, MsgBoxStyle.Information, "~~~HELLO~~~")
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles btn_PengNmSkolah.Click
        btn_PengNmSkolah.Enabled = False
        FormAturSekolah.Show()
    End Sub
    Private Sub TimerCountAndCheck_Tick(sender As Object, e As EventArgs) Handles TimerCountAndCheck.Tick
        GetHariTxt.Text = "Hari/Tanggal/Tahun :   " & conv_tanggal(Format(CDate(Now.Date), "dddd/MM/yyyy"))
        TimerCount1.Text = "Waktu Saat ini  :    " & Format(CDate(Now.ToLongTimeString), "h:mm:ss tt")
        EXTRACT_DATA.Label1.Text = Format(CDate(Now.ToShortTimeString), "h:mm")
    End Sub

    Private Sub Baru_(sender As Object, e As EventArgs) Handles btnBaru.Click
        FormAddSched.Show()
        btnBaru.Enabled = False
    End Sub

    Private Sub FormMain_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.TopMost = True
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Button4.Enabled = False
        FormUbahLogo.Show()
        FormUbahLogo.TopMost = True
    End Sub

    Private Sub ContenTCLICKDATAGRID(
                                                sender As Object, e As DataGridViewCellEventArgs
                                                 ) Handles Datagrid_Jadwal.CellClick
        IDSTRING = String.Empty
        Dim indx As Integer = e.RowIndex
        Dim selectrow As DataGridViewRow
        selectrow = Datagrid_Jadwal.Rows(indx)
        IDSTRING = selectrow.Cells(0).Value.ToString()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If IDSTRING <> "" Then
            KONDISIVALUE = True
            Button1.Enabled = False
            FormAddSched.Show()
            FormAddSched.Btn_TambahJ.Text = "Ubah Data"
        Else
            MsgBox("Belum Ada Data Yang Anda Pilih" & vbNewLine &
                   "Silahkan Pilih data yang ada pada Grid", MsgBoxStyle.Exclamation, "~~~Value Kosong~~~")
        End If
    End Sub

    Private Sub Btn_CopyData_Click(sender As Object, e As EventArgs) Handles Btn_CopyData.Click
        If IDSTRING <> "" Then
            KONDISIVALUE = True
            ParameterCopyFile = "IM BUTTON COPY"
            FormAddSched.Show()
            Btn_CopyData.Enabled = False
        Else
            MsgBox("Belum Ada Data Yang Anda Pilih" & vbNewLine &
                      "Silahkan Pilih data yang ada pada Grid", MsgBoxStyle.Exclamation, "~~~Value Kosong~~~")
        End If


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Button2.Enabled = False
        If IDSTRING = String.Empty Then
            MsgBox("Belum Ada Data Yang Anda Pilih" & vbNewLine &
                     "Silahkan Pilih data yang ada pada Grid", MsgBoxStyle.Exclamation, "~~~Value Kosong~~~")
        Else
            Try
                conn.Open()
                query = "DELETE FROM `jadwal` WHERE id_jadwal='" & IDSTRING & "'"
                cmnd = New MySqlCommand(query, conn)
                Dim q = MessageBox.Show("Hapus Data ID " & IDSTRING, "Hapus Data?", MessageBoxButtons.OKCancel,
                                        MessageBoxIcon.Question)
                If q = Windows.Forms.DialogResult.OK Then
                    cmnd.ExecuteReader()
                    KONDISIVALUE = True
                    MsgBox("Data " & IDSTRING & " Berhasil dihapus..", MsgBoxStyle.Information, "~~ Data telah dihapus ~~")
                Else
                    Datagrid_Jadwal.ClearSelection()
                    IDSTRING = String.Empty
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "")
            Finally
                conn.Close()
                If KONDISIVALUE = True Then
                    TampilkanData()
                    Datagrid_Jadwal.ClearSelection()
                    KONDISIVALUE = False
                End If
            End Try
        End If
        Button2.Enabled = True
    End Sub

    Private Sub btn_backup_Click(sender As Object, e As EventArgs) Handles btn_backup.Click
        b_dBs.Show()
        btn_backup.Enabled = False
    End Sub

    Private Sub btn_restoreDB_Click(sender As Object, e As EventArgs) Handles btn_restoreDB.Click
        RestoreDB.Show()
        btn_restoreDB.Enabled = False
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Dim Check As Boolean = False
        Try
            MsgBox("https://www.facebook.com/poetralesana", MsgBoxStyle.Information, "")
            Dim Q = MessageBox.Show("Kunjungi link?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If Q = Windows.Forms.DialogResult.Yes Then
                Process.Start("https://www.facebook.com/poetralesana")
                Check = True
            End If
        Catch ex As Exception
            If Check = True Then
                Process.Start("iexplore.exe", "https://www.facebook.com/poetralesana")
            End If
        End Try
    End Sub

    Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        Dim Check As Boolean = False
        Try
            MsgBox("https://www.youtube.com/channel/UCesKL9YMfIjKaf7JysGmAQQ", MsgBoxStyle.Information, "~571m Corp Channel~")
            Dim Q = MessageBox.Show("Kunjungi link?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If Q = Windows.Forms.DialogResult.Yes Then
                Process.Start("https://www.youtube.com/channel/UCesKL9YMfIjKaf7JysGmAQQ")
                Check = True
            End If
        Catch ex As Exception
            If Check = True Then
                Process.Start("iexplore.exe", "https://www.youtube.com/channel/UCesKL9YMfIjKaf7JysGmAQQ")
            End If
        End Try
    End Sub

    Private Sub LinkLabel3_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        MessageBox.Show("poetralesana.vbproj@gmail.com", "Email Contact",
                        MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub LinkLabel4_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel4.LinkClicked
        Donasi.TopMost = True
        Donasi.ShowDialog()
    End Sub
End Class
