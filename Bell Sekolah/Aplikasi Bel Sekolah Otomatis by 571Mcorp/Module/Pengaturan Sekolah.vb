﻿Imports MySql.Data.MySqlClient
Module Pengaturan_Sekolah
    Public Sub ValidasiLogoExistOrNo()
        ' {VALIDASI LOGO SEKOLAH}
        With Main.PictApp
            .SizeMode = PictureBoxSizeMode.StretchImage
            If AMBILNAMA_LOGO <> "" Then
                Dim p = Application.StartupPath & "\img\" & AMBILNAMA_LOGO
                .ImageLocation = p
            Else
                .Image = My.Resources.NoIMG
            End If
        End With
        ' --------------------------
    End Sub

    Public Sub GetNamaANDLogoSekolah() 'Ambil Variable Nama Dan Logo Sekolah
        Try
            conn.Open()
            query = "SELECT * FROM `sekolah`"
            cmnd = New MySqlCommand(query, conn)

            readDT = cmnd.ExecuteReader
            With readDT
                readDT.Read() 'baca field comand query

                If .HasRows = True Then
                    AMBILNAMASEKOLAH = .GetString("nama_sekolah").ToUpper 'ambil nama sekolah
                    AMBILNAMA_LOGO = .GetString("logo") 'AmbilNamaLogo
                    AMBILJALANSEKOLAH = StrConv(.GetString("jalan_sekolah"), VbStrConv.ProperCase) 'ambil jalan
                    ValidasiLogoExistOrNo()
                Else
                    AMBILNAMASEKOLAH = "Nama Sekolah Anda".ToUpper
                    AMBILJALANSEKOLAH = "Nama Jalan".ToUpper
                End If
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "")
        Finally
            readDT.Close()
            conn.Close()
        End Try
    End Sub
    Public Sub UpdateNamaSekolah()
        KONDISIVALUE = False
        Dim QST = MessageBox.Show("Ubah Data..?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If QST = DialogResult.Yes Then
            Try
                conn.Open()
                query = "UPDATE `sekolah` SET `nama_sekolah`='" & AMBILNAMASEKOLAH.ToUpper & "',`jalan_sekolah`='" & StrConv(
                                                                AMBILJALANSEKOLAH, VbStrConv.ProperCase) & "' WHERE id=1"
                cmnd = New MySqlCommand(query, conn)
                cmnd.ExecuteReader()
                KONDISIVALUE = True ' Jika Berubah =true,,, Maka, Pada FormMain Nama Akan Berubah.
                MsgBox("Data Berhasil Diubah..", MsgBoxStyle.Information, "")
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "")
            Finally
                conn.Close()
            End Try
        End If
    End Sub
End Module
