﻿Module Prosses2
    Dim x As New FormAddSched
    Public Function UbahData(Desktipsi As TextBox, Jam As ComboBox, menit As ComboBox,
                             pmam As ComboBox, hari As GroupBox, nama_nada As ComboBox, ID_jadwal As Label, ByVal Button1 As Button)

        Dim RbV As RadioButton
        Dim NameItem As String = ""
        'get hari
        For Each RbV In hari.Controls
            If RbV.Checked = True Then
                NameItem = RbV.Text
            End If
        Next
        Dim Result_ As Boolean = False
        Try
            With x
                conn.Open()
                query = "UPDATE `jadwal` SET `deskripsi`='" & .EscP(Desktipsi.Text) & "',`jam`='" & .EscP(Jam.Text) & "',`menit`='" & .EscP(menit.Text) & "',`pmam`='" & .EscP(pmam.Text) & "',`hari`='" & .EscP(NameItem) & "',`nama_nada`='" & .EscP(nama_nada.Text) & "' WHERE id_jadwal='" & .EscP(ID_jadwal.Text) & "'"
                cmnd = New MySql.Data.MySqlClient.MySqlCommand(query, conn)
                Dim q = MessageBox.Show("Ubah Data..?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If q = DialogResult.Yes Then
                    cmnd.ExecuteReader()
                    Result_ = True
                    Button1.Enabled = True
                    MsgBox("Data Berhasil Diubah...", MsgBoxStyle.Information, "")
                End If
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "")
            Application.Exit()
        Finally
            conn.Close()
            If Result_ = True Then
                Main.TampilkanData()
            End If
        End Try
        Return True
    End Function
End Module
