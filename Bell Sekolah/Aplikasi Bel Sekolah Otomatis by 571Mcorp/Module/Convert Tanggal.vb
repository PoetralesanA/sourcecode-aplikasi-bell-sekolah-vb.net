﻿Module Convert_Tanggal
    Public Function conv_tanggal(ByVal tgl As String) 'Manipulasi Tanggal
        Return Replace(Replace(Replace(Replace(Replace(Replace(
                Replace(tgl,
                       "Sunday", "Minggu"),
                       "Monday", "Senin"),
                       "Tuesday", "Selasa"),
                       "Wednesday", "Rabu"),
                       "Thursday", "Kamis"),
                       "Friday", "Jumat"),
                       "Saturday", "Sabtu")
    End Function
End Module
